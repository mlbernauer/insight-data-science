#!/usr/bin/env bash

# Run average_degree.py using tweet_input/tweets.txt as input and ave results
# to ./tweet_output/output.txt
python ./src/average_degree.py ./tweet_input/tweets.txt ./tweet_output/output.txt
