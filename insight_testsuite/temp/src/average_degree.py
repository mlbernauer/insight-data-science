from TweetGraph import TweetGraph
import sys

# Create TweetGraph object
tg = TweetGraph()

# Create iterable object
stream = tg.read_stream(sys.argv[1])

# Create file for saving results
outfile = open(sys.argv[2], 'w')

# Iterate over iterable and write to output file
for i in stream:
    outfile.write("%.2f\n" % i)

# Close the output file
outfile.close()
