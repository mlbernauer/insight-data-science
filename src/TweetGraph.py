import json
import time

class TweetGraph:
    """Object used for computing average degree from a twitter hashtag graph."""    
    def __init__(self):
        # Keeps track of the most current created_at added to edge list
        self.max_time = 0
        # Keeps track of the oldest created_at added to edge list
        self.min_time = 1e10
        # List for storing edges that fall within window
        self.edges = []
        # Variable for holding the mean degree value
        self.degree = 0
    
    def read_stream(self, handle):
        """This function creates an interable object from the twitter stream instead of loading
           everything into memory. Tweets are processed one at a time for scalability.
        """
        # Create the iterable stream
        stream = iter(open(handle))
        # Iterate over twitter stream
        for tweet in stream:
            # Read json format
            data = json.loads(tweet)
            # Continue on rate limit error
            if 'limit' in data: continue
            # Extract timestamp
            timestamp = time.mktime(time.strptime(data['created_at'], '%a %b %d %H:%M:%S +0000 %Y'))
            # Extract unique hashtags
            hashtags = list(set([i['text'] for i in data['entities']['hashtags']]))
            # Update max_time and all related variables
            if timestamp > self.max_time:
                # Update max_time
                self.max_time = timestamp
                # timerange is used to keep track of oldes and newest tweets in edge list.
                # We will remove old tweets only when timerange >= 60.0
                timerange = self.max_time - self.min_time
                # Delta is the timestamp for the oldest allowable tweet. It is the lower boundary of our
                # 60s window.
                delta = self.max_time - 60.0
            # If timerange > 60s then prune the graph by removing old edges
            if timerange > 60.0:
                # Remove old edges
                self.edges = [i for i in self.edges if i['timestamp'] >= delta]    
                # Since edge list updated we need to recompute average degree
                if len(self.edges) > 0:
                    self.degree = self.compute_degree()
            # If tweet is within time window update min_time and timerange. Also add edges to 
            # list if more than one unique hashtag.
            if timestamp >= delta:
                # Update min_time
                if timestamp < self.min_time:
                    # Update min_time
                    self.min_time = timestamp
                    # Since min_time was updated, we must also update timerange
                    timerange = self.max_time - self.min_time
                # Add edges to list if more than one unique hashtag
                if len(hashtags) > 1:
                    # Update edges
                    self.edges.append({'timestamp':timestamp, 'edges':self.get_edges(hashtags)})
                    # Since edge list was updated we must update degree
                    self.degree = self.compute_degree()
            # Return degree calculation for each tweet observed. Does not return value for limit errors.
            yield self.degree
                
    def get_edges(self, hashtags):
        """Given a list of unique hashtags returns a list of tuples where each tuple is a
           unique edge.
        """
        # Compute number of unique hashtags
        n = len(hashtags)
        # Return list of tuples containing unique edges from hashtags
        return [tuple(sorted([hashtags[i], hashtags[j]])) for i in range(n) for j in range(i+1,n)]         
        
    def compute_degree(self):
        """This compute the average degree from edge list."""
        # Create set of unique edges
        edges = set(j for i in self.edges for j in i['edges'])
        # Create set of unique nodes (i.e. hashtag terms)
        nodes = set(j for i in edges for j in i)
        # Compute degree using the formula 2*num_edges / num_nodes
        num_edges = len(edges)
        num_nodes = len(nodes)
        degree = (2*num_edges)/float(num_nodes)
        # Return degree with required precision
        return degree
