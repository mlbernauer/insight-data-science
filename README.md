# Insight Data Engineering - Coding Challenge
_by [Michael L. Bernauer](http://mlbernauer.com)_

## Dependencies
This application was implemented in Python and requries the following libraries

1. `json`
2. `time`

## Description
This challenge requires the calculation of the average degree of a vertex of a Twitter hashtag graph for a 60 second sliding window.
This value is updated each time a new tweet appears.

## Solution
A TwitterGraph object is created which contains a method for attaching to the Twitter API stream (or in this case a `tweets.txt` file).
Each line in this file is iterated over and processed individually to avoid using too much memory. The line is first checked to see if
it is a rate limit error, if so, it is thrown out and the the next line is read. If it is determined to be a tweet then the `created_at` value
is compared to `max_time` and `min_time` variables which store the maximum and minimum boudaries for tweets in our 60s window. If `created_at` is
larger than `max_time` then we update `max_time` by setting it equal to `created_at`. We do the same for `min_time`. Once these values are udpated, we
must also update all related variables such as `delta` and `timerange`. `delta` is computed accourding to `max_time - 60.0` and represents the absolute
oldest a tweet can be in order to be stored in our `edges` list. If a tweet's `created_at` value is larger than `delta` we will add it to the graph by
storing it in `edges`. `timerange` is used to keep track of the difference in seconds betweet the newest and oldest tweets in our graph. This variable
is used to let us know when we should remove old tweets from our `edges` list. If `timerange` is greater than 60s this means that we have a tweet that
has fallen outside of our 60s window and should be removed. As a result, we run 'prune' our `edges` list by removing all old tweets whos `created_by` are
less than `delta`. From the `edges` list we can then compute the average degree. The method `compute_degree()` is used to compute the average degree using
the `edge` list and is used to set the variable `degree` which stores the rolling average. `compute_degree()` is called any time we make a change to the `edge`
list as this will likely result in an update to `degree`. Only tweets containing more than one unique hashtag are appended to the `edges` list. However, tweets
containing zero or one hashtags can update the `max_time`, `min_time` variable and thus influence `delta` and `timerange`.

## Usage
```
# import class
from TweetGraph import TweetGraph

# create TweetGraph object
t = TweetGraph()

# create an iterable stream
stream = t.read_stream('./tweets_input/tweets.txt')

# save the output from stream to output file
outfile = open('./tweets_output/output.txt')

# iterate and save result to file
for i in stream:
  outfile.write('%.2f\n' % i)

# close the output file
outfile.close()
```
